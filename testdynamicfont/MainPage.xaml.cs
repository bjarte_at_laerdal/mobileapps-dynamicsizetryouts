﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace testdynamicfont
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new ViewM();

        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height); //must be called

            ((ViewM)BindingContext).S = width * 0.05;
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            App.Current.MainPage = new MyPage();
        }
    }

    public class ViewM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private double _s;
        public double S
        {
            get => _s;
            set
            {
                _s = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("S"));
            }
        }
    }
}
