﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace Laerdal.Accessibility
{
    public static class FontSize
    {
        public static readonly BindableProperty DefaultFontSizeProperty =
            BindableProperty.CreateAttached("DefaultFontSize", typeof(double), typeof(FontSize), 0.0, propertyChanged: DefaultChanged);

        public static double GetDefaultFontSize(BindableObject view)
        {
            return (double)view.GetValue(DefaultFontSizeProperty);
        }

        private static void DefaultChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as View;
            if (view == null)
            {
                return;
            }

            var effect = view.Effects.FirstOrDefault(e => e is DynamicSizeEffect);
            if(effect is null)
            {
                effect = new DynamicSizeEffect();
                view.Effects.Add(effect);
            }
        }

        class DynamicSizeEffect : RoutingEffect
        {
            public DynamicSizeEffect() : base("Laerdal.Accessibility.DynamicSizeEffect")
            {

            }
        }
    }
}
