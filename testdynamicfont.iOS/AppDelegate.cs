﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace testdynamicfont.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            PrintStyles();

            return base.FinishedLaunching(app, options);
        }

        private void PrintStyles()
        {
            PrintStyle("Body", UIFont.PreferredBody);
            PrintStyle("Callout", UIFont.PreferredCallout);
            PrintStyle("Caption1", UIFont.PreferredCaption1);
            PrintStyle("Caption2", UIFont.PreferredCaption2);  ;
            PrintStyle("Footnote", UIFont.PreferredFootnote);
            PrintStyle("Headline", UIFont.PreferredHeadline);
            PrintStyle("SubheadLine", UIFont.PreferredSubheadline);
            PrintStyle("Title1", UIFont.PreferredTitle1);
            PrintStyle("Title2", UIFont.PreferredTitle2);
            PrintStyle("Title3", UIFont.PreferredTitle3);
        }

        private void PrintStyle(string name, UIFont preferredBody)
        {
            Console.WriteLine(name + ": " + preferredBody.Name + ", " + preferredBody.PointSize);
        }
    }
}
