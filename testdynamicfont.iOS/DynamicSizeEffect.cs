﻿using System;
using System.ComponentModel;
using Foundation;
using testdynamicfont.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("Laerdal.Accessibility")]
[assembly: ExportEffect(typeof(DynamicSizeEffect), nameof(DynamicSizeEffect))]
namespace testdynamicfont.iOS
{
    public class DynamicSizeEffect : PlatformEffect
    {
        NSObject _observer;

        protected override void OnAttached()
        {
            UpdateSize();

            _observer = NSNotificationCenter.DefaultCenter.AddObserver(
                    new NSString("UIContentSizeCategoryDidChangeNotification"),
                    (NSNotification obj) =>
                    {
                        UpdateSize();
                    },
                    null);
        }

        protected override void OnDetached()
        {
            NSNotificationCenter.DefaultCenter.RemoveObserver(_observer);
            _observer?.Dispose();
        }

        void UpdateSize()
        {
            var pointSize = UIFontDescriptor.PreferredBody.PointSize;
            // 17 is the "standard" value for preferredBody 
            var scaleRatio = pointSize / 17;

            var p = Element.GetType().GetProperty("FontSize");
            if (p != null)
            {
                p.SetValue(Element, scaleRatio * Laerdal.Accessibility.FontSize.GetDefaultFontSize(Element));
            }
        }

        protected override void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            if(args.PropertyName == "DefaultFontSize")
            {
                UpdateSize();
            }
        }
    }
}
